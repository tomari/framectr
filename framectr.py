#!/usr/bin/python
## Frame counter. 2014 H.Tomari.
## Reused parts of PyQt4 examples:
## Copyright (C) 2010 Riverbank Computing Limited.
## Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
## All rights reserved.
## 
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
from PyQt4 import QtCore, QtGui

class Button(QtGui.QToolButton):
    def __init__(self, text, parent=None):
        super(Button, self).__init__(parent)
        
        self.setSizePolicy(QtGui.QSizePolicy.Expanding,
                QtGui.QSizePolicy.Preferred)
        self.setText(text)
        
    def sizeHint(self):
        size = super(Button, self).sizeHint()
        size.setHeight(size.height() + 20)
        size.setWidth(max(size.width(), size.height()))
        return size

class FrameCounter(QtGui.QDialog):
    def __init__(self, parent=None):
        super(FrameCounter,self).__init__(parent)
        
        self.timer=QtCore.QTimer(self)
        self.timer.timeout.connect(self.showTime)
        
        self.display=QtGui.QLineEdit('Java')
        self.display.setReadOnly(True)
        self.display.setAlignment(QtCore.Qt.AlignRight)
        font=self.display.font()
        font.setPointSize(font.pointSize()*4)
        self.display.setFont(font)
        self.resetPressed()
        
        self.resetButton=Button("Reset")
        self.resetButton.clicked.connect(self.resetPressed)
        
        self.startStopButton=Button("Start/Stop")
        self.startStopButton.clicked.connect(self.startStopPressed)
        
        mainLayout=QtGui.QGridLayout()
        mainLayout.addWidget(self.display,0,0,1,2)
        mainLayout.addWidget(self.resetButton,1,0,1,1)
        mainLayout.addWidget(self.startStopButton,1,1,1,1)
        self.setLayout(mainLayout)
        self.setWindowTitle("FrameCounter")
        
    def resetPressed(self):
        self.startTime=QtCore.QTime.currentTime()
        self.stopTime=None
        self.showTime()
        return
        
    def startStopPressed(self):
        if(self.timer.isActive()):
            self.timer.stop()
            self.stopTime=QtCore.QTime.currentTime()
        else:
            now=QtCore.QTime.currentTime()
            if(self.stopTime!=None):
                ms=self.stopTime.msecsTo(now)
                self.startTime=self.startTime.addMSecs(ms)
            else:
                self.startTime=now
            self.timer.start(1000/60)
        return
        
    def showTime(self):
        if(self.timer.isActive()):
            msec=self.startTime.elapsed()
        else:
            msec=0
        seconds=int(msec/1000)
        msec_str='%03d'%(msec-seconds*1000)
        minutes=int(seconds/60)
        seconds_str='%02d'%(seconds-minutes*60)
        hours=int(minutes/60)
        minutes_str='%02d'%(minutes-hours*60)
        self.display.setText(str(hours)+":"+
                             minutes_str+":"+
                             seconds_str+"."+
                             msec_str)

if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    fctr = FrameCounter()
    sys.exit(fctr.exec_())
